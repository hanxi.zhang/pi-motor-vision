import numpy as np
import cv2
import sys
import time
import _thread
from time import sleep
import RPi.GPIO as GPIO
from gpiozero import LineSensor

left_sensor = LineSensor(27)
right_sensor = LineSensor(17)
state = None

Motor1A = 16
Motor1B = 18
Motor1E = 22

Motor2A = 19
Motor2B = 21
Motor2E = 23

def init():
	GPIO.setmode(GPIO.BOARD)
		 
	GPIO.setup(Motor1A,GPIO.OUT)
	GPIO.setup(Motor1B,GPIO.OUT)
	GPIO.setup(Motor1E,GPIO.OUT)
	 
	GPIO.setup(Motor2A,GPIO.OUT)
	GPIO.setup(Motor2B,GPIO.OUT)
	GPIO.setup(Motor2E,GPIO.OUT)


def forward(time):
	print ("Going forwards")
	init()
	GPIO.output(Motor1A,GPIO.LOW)
	GPIO.output(Motor1B,GPIO.HIGH)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.LOW)
	GPIO.output(Motor2B,GPIO.HIGH)
	GPIO.output(Motor2E,GPIO.HIGH)

	#sleep(time)
	time.sleep(100.0 * time / 1000.0)
	
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)

def backward(time): 
	print ("Going backwards")
	init()
	GPIO.output(Motor1A,GPIO.HIGH)
	GPIO.output(Motor1B,GPIO.LOW)
	GPIO.output(Motor1E,GPIO.HIGH)
 
	GPIO.output(Motor2A,GPIO.HIGH)
	GPIO.output(Motor2B,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.HIGH)
 
	sleep(time)
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)

def turnleft():
	print ("Going left")
	init()
	GPIO.output(Motor1A,GPIO.HIGH)
	GPIO.output(Motor1B,GPIO.LOW)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.LOW)
	GPIO.output(Motor2B,GPIO.HIGH)
	GPIO.output(Motor2E,GPIO.HIGH)

	time.sleep(400.0 / 1000.0)
	
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)
	#reset()

def turnright():
	print ("Going right")
	init()
	GPIO.output(Motor1A,GPIO.LOW)
	GPIO.output(Motor1B,GPIO.HIGH)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.HIGH)
	GPIO.output(Motor2B,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.HIGH)

	time.sleep(400.0 / 1000.0)

	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)
	#reset()

def reset():
    #init()
    #GPIO.setmode(GPIO.BOARD)
    #GPIO.output(Motor1E,GPIO.LOW)
    #GPIO.output(Motor2E,GPIO.LOW)

    GPIO.cleanup()

def sensor_state():
    return 2*(int(left_sensor.value)) + int(right_sensor.value)

def sensor_control(flag):
    old_state = state
    state = sensor_state()

    if old_state==3 and state==2:
        turnleft()

    elif old_state==3 and state==1:
        turnright()

    else:
        pass    

state = sensor_state()

def myjob():
    reset()
    init()
    sleep(1)
    
    left_sensor.when_line = lambda: sensor_control()
    left_sensor.when_no_line = lambda: sensor_control()
    right_sensor.when_line = lambda: sensor_control()
    right_sensor.when_no_line = lambda: sensor_control()

    for x in range(5):
        forward(1)
        #turnleft()
        sleep(2)
        #turnright()

    reset()

#init()

try:
    _thread.start_new_thread( myjob, ())
except:
    print ("Exception starting thread, quit")
    sys.exit(1)

counter = 0
while counter<=100:
    sleep(1)
    print(left_sensor.value, right_sensor.value)
    counter = counter + 1

reset()
