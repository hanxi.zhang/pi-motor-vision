import numpy as np
import cv2
import sys
import time
import _thread
from time import sleep
import RPi.GPIO as GPIO
#import motor as motor

Motor1A = 16
Motor1B = 18
Motor1E = 22

Motor2A = 19
Motor2B = 21
Motor2E = 23

def init():
	GPIO.setmode(GPIO.BOARD)
		 
	GPIO.setup(Motor1A,GPIO.OUT)
	GPIO.setup(Motor1B,GPIO.OUT)
	GPIO.setup(Motor1E,GPIO.OUT)
	 
	GPIO.setup(Motor2A,GPIO.OUT)
	GPIO.setup(Motor2B,GPIO.OUT)
	GPIO.setup(Motor2E,GPIO.OUT)


def forward(time):
	print ("Going forwards")
	init()
	GPIO.output(Motor1A,GPIO.LOW)
	GPIO.output(Motor1B,GPIO.HIGH)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.LOW)
	GPIO.output(Motor2B,GPIO.HIGH)
	GPIO.output(Motor2E,GPIO.HIGH)

	#sleep(time)
	time.sleep(100.0 * time / 1000.0)
	
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)

def backward(time): 
	print ("Going backwards")
	init()
	GPIO.output(Motor1A,GPIO.HIGH)
	GPIO.output(Motor1B,GPIO.LOW)
	GPIO.output(Motor1E,GPIO.HIGH)
 
	GPIO.output(Motor2A,GPIO.HIGH)
	GPIO.output(Motor2B,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.HIGH)
 
	sleep(time)
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)

def turnleft():
	print ("Going left")
	init()
	GPIO.output(Motor1A,GPIO.HIGH)
	GPIO.output(Motor1B,GPIO.LOW)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.LOW)
	GPIO.output(Motor2B,GPIO.HIGH)
	GPIO.output(Motor2E,GPIO.HIGH)

	time.sleep(400.0 / 1000.0)
	
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)
	#reset()

def turnright():
	print ("Going right")
	init()
	GPIO.output(Motor1A,GPIO.LOW)
	GPIO.output(Motor1B,GPIO.HIGH)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.HIGH)
	GPIO.output(Motor2B,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.HIGH)

	time.sleep(400.0 / 1000.0)

	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)
	#reset()

def reset():
    init()
    GPIO.output(Motor1E,GPIO.LOW)
    GPIO.output(Motor2E,GPIO.LOW)

    GPIO.cleanup()

def myjob():
    reset()
    init()
    sleep(5)
    for x in range(5):
        forward(1)
        #turnleft()
        sleep(2)
        #turnright()

    reset()

try:
    _thread.start_new_thread( myjob, ())
except:
    print ("Exception starting thread, quit")
    sys.exit(1)

video_capture = cv2.VideoCapture(0)
 
while(True):
    try: 
	    # Capture the frames
	    ret, frame = video_capture.read()
	
	    #print("Ret is: " + str(ret) )
	    # Crop the image
	
	    crop_img = frame[60:120, 0:160]
	    #crop_img = frame[80:160, 0:220]
	
	    # Convert to grayscale
	
	    gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
	
	
	    # Gaussian blur
	
	    blur = cv2.GaussianBlur(gray,(5,5),0)
	
	
	    # Color thresholding
	
	    ret,thresh = cv2.threshold(blur,60,255,cv2.THRESH_BINARY_INV)
	
	
	    # Find the contours of the frame
	    contours,hierarchy = cv2.findContours(thresh.copy(), 1, cv2.CHAIN_APPROX_NONE)
	
	    # Find the biggest contour (if detected)
	
	    if len(contours) > 0:
	
	        c = max(contours, key=cv2.contourArea)
	
	        M = cv2.moments(c)
	
	        if(M['m00'] == 0):
	            continue
	
	        cx = int(M['m10']/M['m00'])
	
	        cy = int(M['m01']/M['m00'])
	
	
	        cv2.line(crop_img,(cx,0),(cx,720),(255,0,0),1)
	
	        cv2.line(crop_img,(0,cy),(1280,cy),(255,0,0),1)
	
	
	        cv2.drawContours(crop_img, contours, -1, (0,255,0), 1)
	
	
	        if cx >= 120:
	
	            #print("Turn Right >>>>>>>>>>")
	            turnright()
	
	        if cx < 120 and cx > 50:
	
	            print("On Track ----------")
	
	 
	        if cx <= 50:
	
	            #print("Turn Left <<<<<<<<<<")
	            turnleft()
	    else:

	    	print("No line detected ??????????")
	
	    #sleep(1)	
	    cv2.imshow('frame',crop_img)

	    if cv2.waitKey(1) & 0xFF == ord('q'):
	        sys.exit

    except KeyboardInterrupt:
        print("KeyboardInterrupt, cleaning up...")
        reset()
        sys.exit
