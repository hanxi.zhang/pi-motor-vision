from gpiozero import Robot, LineSensor
from time import sleep

#robot = Robot(left=(7, 8), right=(9, 10))
left_sensor = LineSensor(27)
right_sensor= LineSensor(17)

while True:
	left_detect  = int(left_sensor.value)
	right_detect = int(right_sensor.value)
	print('[' + str(left_detect) + ', ' + str(right_detect) + ']') 
	sleep(1)
