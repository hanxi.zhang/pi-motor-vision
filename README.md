Misc code snippets for a Raspberry Pi project William and I did in Jan/Feb 2019. A line-following robot was implemented in two different approaches:

1. Raspberry Pi Camera and opencv computer vision library

2. A pair of infrared sensors. 

A few project videos on youtube:

    https://www.youtube.com/watch?v=3mT4ei3Vr0I

    https://www.youtube.com/watch?v=oHHGvKm1b9E

    https://www.youtube.com/watch?v=80Oye6_1O0A

    https://www.youtube.com/watch?v=YcVVfOalVOI

