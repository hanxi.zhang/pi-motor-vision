import numpy as np
import cv2
import sys
import time
import _thread
from time import sleep
import RPi.GPIO as GPIO
from gpiozero import LineSensor

Motor1A = 23
Motor1B = 24
Motor1E = 25

Motor2A = 10
Motor2B = 9
Motor2E = 11

def init():
	GPIO.setmode(GPIO.BCM)
		 
	GPIO.setup(Motor1A,GPIO.OUT)
	GPIO.setup(Motor1B,GPIO.OUT)
	GPIO.setup(Motor1E,GPIO.OUT)
	 
	GPIO.setup(Motor2A,GPIO.OUT)
	GPIO.setup(Motor2B,GPIO.OUT)
	GPIO.setup(Motor2E,GPIO.OUT)

def forward(time):
	print ("Going forwards")
	init()
	GPIO.output(Motor1A,GPIO.LOW)
	GPIO.output(Motor1B,GPIO.HIGH)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.LOW)
	GPIO.output(Motor2B,GPIO.HIGH)
	GPIO.output(Motor2E,GPIO.HIGH)

	sleep(time)
	#time.sleep(100.0 * time / 1000.0)
	
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)

def backward(time): 
	print ("Going backwards")
	init()
	GPIO.output(Motor1A,GPIO.HIGH)
	GPIO.output(Motor1B,GPIO.LOW)
	GPIO.output(Motor1E,GPIO.HIGH)
 
	GPIO.output(Motor2A,GPIO.HIGH)
	GPIO.output(Motor2B,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.HIGH)
 
	sleep(time)
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)

def turnleft():
	print ("Going left")
	init()
	GPIO.output(Motor1A,GPIO.HIGH)
	GPIO.output(Motor1B,GPIO.LOW)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.LOW)
	GPIO.output(Motor2B,GPIO.HIGH)
	GPIO.output(Motor2E,GPIO.HIGH)

	time.sleep(400.0 / 1000.0)
	
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)
	#reset()

def turnright():
	print ("Going right")
	init()
	GPIO.output(Motor1A,GPIO.LOW)
	GPIO.output(Motor1B,GPIO.HIGH)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.HIGH)
	GPIO.output(Motor2B,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.HIGH)

	time.sleep(400.0 / 1000.0)

	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)
	#reset()

def reset():
    init()
    GPIO.output(Motor1E,GPIO.LOW)
    GPIO.output(Motor2E,GPIO.LOW)

    GPIO.cleanup()

#def sensor_state():
#    return 2*(int(left_sensor.value)) + int(right_sensor.value)

'''
def sensor_control():
    old_state = state
    state = sensor_state()

    if old_state==3 and state==2:
        turnleft()

    elif old_state==3 and state==1:
        turnright()

    else:
        pass    
'''
#state = sensor_state()

def myjob():
    reset()
    init()
    sleep(1)

    for x in range(5):
        forward(7)
        sleep(2)

    reset()

try:
    _thread.start_new_thread( myjob, ())
except:
    print ("Exception starting thread, quit")
    sys.exit(1)

left_sensor = LineSensor(27)
right_sensor = LineSensor(17)

state = None
counter = 0
while counter<=100:
    if left_sensor != None and right_sensor != None:
        gouche  = int(left_sensor.value)
        droit = int(right_sensor.value)
        print(gouche, droit)

        old_state = state
        state = 2*gouche + droit

        if old_state == 0 and state == 2:
            turnleft()
        elif old_state == 0 and state == 1:
            turnright()
        else:
            pass

    time.sleep(400.0 / 1000.0)

    counter = counter + 1

reset()

