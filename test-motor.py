import RPi.GPIO as GPIO
from time import sleep
import time
import _thread

Motor1A = 16
Motor1B = 18
Motor1E = 22

Motor2A = 19
Motor2B = 21
Motor2E = 23

def init():
	GPIO.setmode(GPIO.BOARD)
		 
	GPIO.setup(Motor1A,GPIO.OUT)
	GPIO.setup(Motor1B,GPIO.OUT)
	GPIO.setup(Motor1E,GPIO.OUT)
	 
	GPIO.setup(Motor2A,GPIO.OUT)
	GPIO.setup(Motor2B,GPIO.OUT)
	GPIO.setup(Motor2E,GPIO.OUT)


def forward(time):
	print ("Going forwards")
	#init()
	GPIO.output(Motor1A,GPIO.LOW)
	GPIO.output(Motor1B,GPIO.HIGH)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.LOW)
	GPIO.output(Motor2B,GPIO.HIGH)
	GPIO.output(Motor2E,GPIO.HIGH)

	sleep(time)
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)

def backward(time): 
	print ("Going backwards")
	#init()
	GPIO.output(Motor1A,GPIO.HIGH)
	GPIO.output(Motor1B,GPIO.LOW)
	GPIO.output(Motor1E,GPIO.HIGH)
 
	GPIO.output(Motor2A,GPIO.HIGH)
	GPIO.output(Motor2B,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.HIGH)
 
	sleep(time)
	GPIO.output(Motor1E,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.LOW)

def turnleft():
	print ("Going left")
	#init()
	GPIO.output(Motor1A,GPIO.HIGH)
	GPIO.output(Motor1B,GPIO.LOW)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.LOW)
	GPIO.output(Motor2B,GPIO.HIGH)
	GPIO.output(Motor2E,GPIO.HIGH)

	time.sleep(400.0 / 1000.0)
	
	#GPIO.output(Motor1E,GPIO.LOW)
	#GPIO.output(Motor2E,GPIO.LOW)
	#reset()

def turnright():
	print ("Going right")
	#init()
	GPIO.output(Motor1A,GPIO.LOW)
	GPIO.output(Motor1B,GPIO.HIGH)
	GPIO.output(Motor1E,GPIO.HIGH)

	GPIO.output(Motor2A,GPIO.HIGH)
	GPIO.output(Motor2B,GPIO.LOW)
	GPIO.output(Motor2E,GPIO.HIGH)

	time.sleep(400.0 / 1000.0)

	#GPIO.output(Motor1E,GPIO.LOW)
	#GPIO.output(Motor2E,GPIO.LOW)
	#reset()

def reset():
    init()
    GPIO.output(Motor1E,GPIO.LOW)
    GPIO.output(Motor2E,GPIO.LOW)

    GPIO.cleanup()

def myjob():
    init()
    for y in range(4):
        forward(1)
        sleep(1)

sleep(2)
try:
    _thread.start_new_thread( myjob, ())
except:
    print ("Exception starting thread!")


for x in range(10):
    print("in loop " + str(x) + " ......")
    sleep(1)

sleep(3)

for x in range(5):
#   turnleft()
    forward(1)
    sleep(1)
#turnright()
#backward(1)
#sleep(1)

reset()
